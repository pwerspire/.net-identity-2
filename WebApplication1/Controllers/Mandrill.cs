﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Configuration;
using System.Dynamic;
using EasyHttp.Http;
using System.Net;
using Microsoft.AspNet.Identity;

namespace WebApplication1.Controllers
{
    enum MandrillError
    {
        OK,
        WebException,
        HttpNotOk,
        Invalid,
        Rejected,
        Unknown
    }

    
    public class Mandrill
    {
        static string MandrillBaseUrl = ConfigurationManager.AppSettings["MandrillBaseUrl"];
        static string MandrillKey = ConfigurationManager.AppSettings["MandrillKey"];
        
        
        public bool SendActivationEMail(string activationLink, IdentityMessage message)
        {
           

            //send-template(string key, string template_name, array template_content, struct message) 
            dynamic sendParams = new ExpandoObject();
            sendParams.key = MandrillKey;
            sendParams.template_name = "Account-Confirmation";

            sendParams.template_content = new List<dynamic>();

            sendParams.message = new ExpandoObject();
            sendParams.message.subject = "Absyla Account Confirmation";
            sendParams.message.from_email = "pwerspire@gmail.com";
           sendParams.message.from_name = "Absyla";

            sendParams.message.to = new List<dynamic>();
            sendParams.message.to.Add(new ExpandoObject());
            sendParams.message.to[0].email = message.Destination;
           // sendParams.message.to[0].name = Name;

            sendParams.message.track_opens = true;
            //sendParams.message.track_clicks = true;

            // merge tags in mandrill template:
            sendParams.message.global_merge_vars = new List<dynamic>();
            sendParams.message.global_merge_vars.Add(new ExpandoObject());
            sendParams.message.global_merge_vars[0].name = "ACTIVATION_CODE";
            sendParams.message.global_merge_vars[0].content = message.Body;

            //sendParams.message.global_merge_vars.Add(new ExpandoObject());
            //sendParams.message.global_merge_vars[1].name = "LINK";
            //sendParams.message.global_merge_vars[1].content = activationLink;

           // errorMsg = string.Empty;

            MandrillError merr = SendMessage(sendParams);

            switch (merr)
            {
                case MandrillError.OK:
                    return true;
                case MandrillError.WebException:
                case MandrillError.HttpNotOk:
                    //errorMsg = "There was an issue sending your activation e-mail. Please try again later or call us directly.";
                    break;
                case MandrillError.Invalid:
                    //errorMsg = "Your email address appears to be invalid. Please try again with a valid address, or call us directly.";
                    break;
                case MandrillError.Rejected:
                   // errorMsg = "Your activation email was rejected. Please try again with a valid address, or call us directly.";
                    break;
                case MandrillError.Unknown:
                    //errorMsg = "There was an unknown problem sending your activation email. Please try again, or call us directly.";
                    break;
            }
            return false;
        }

        public bool SendResetPasswordEMail(string activationLink, IdentityMessage message)
        {


            //send-template(string key, string template_name, array template_content, struct message) 
            dynamic sendParams = new ExpandoObject();
            sendParams.key = MandrillKey;
            sendParams.template_name = "Reset-Password";

            sendParams.template_content = new List<dynamic>();

            sendParams.message = new ExpandoObject();
            sendParams.message.subject = "Absyla Reset Password";
            sendParams.message.from_email = "pwerspire@gmail.com";
            sendParams.message.from_name = "Absyla";

            sendParams.message.to = new List<dynamic>();
            sendParams.message.to.Add(new ExpandoObject());
            sendParams.message.to[0].email = message.Destination;
           

            sendParams.message.track_opens = true;
            //sendParams.message.track_clicks = true;

            // merge tags in mandrill template:
            sendParams.message.global_merge_vars = new List<dynamic>();
            sendParams.message.global_merge_vars.Add(new ExpandoObject());
            sendParams.message.global_merge_vars[0].name = "ACTIVATION_CODE";
            sendParams.message.global_merge_vars[0].content = message.Body;


            // errorMsg = string.Empty;

            MandrillError merr = SendMessage(sendParams);

            switch (merr)
            {
                case MandrillError.OK:
                    return true;
                case MandrillError.WebException:
                case MandrillError.HttpNotOk:
                    //errorMsg = "There was an issue sending your activation e-mail. Please try again later or call us directly.";
                    break;
                case MandrillError.Invalid:
                    //errorMsg = "Your email address appears to be invalid. Please try again with a valid address, or call us directly.";
                    break;
                case MandrillError.Rejected:
                    // errorMsg = "Your activation email was rejected. Please try again with a valid address, or call us directly.";
                    break;
                case MandrillError.Unknown:
                    //errorMsg = "There was an unknown problem sending your activation email. Please try again, or call us directly.";
                    break;
            }
            return false;
        }
       

        private static MandrillError SendMessage(dynamic sendParams)
        {
          
            string url = MandrillBaseUrl + "/messages/send-template.json";

            var http = new EasyHttp.Http.HttpClient
            {
                Request = { Accept = HttpContentTypes.ApplicationJson }
            };

            EasyHttp.Http.HttpResponse response;
            try
            {
                response = http.Post(url, sendParams, HttpContentTypes.ApplicationJson);
            }
            catch (WebException ex)
            {
                //_log.ErrorFormat("Error: WebException - {0}", ex.Message);
                return MandrillError.WebException;
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                //_log.InfoFormat("Response = {0} - {1}", response.StatusCode, response.StatusDescription);
                //_log.Info(response.RawText);
                return MandrillError.HttpNotOk;
            }

            dynamic rv = response.DynamicBody;
            //_log.InfoFormat("email: {0}, status: {1}", rv[0].email, rv[0].status);

            string send_status = rv[0].status;
            if (send_status == "sent" || send_status == "queued")
                return MandrillError.OK;

            // otherwise, it should be "rejected" or "invalid"
            if (send_status == "invalid")
            {
                return MandrillError.Invalid;
            }
            if (send_status == "rejected")
            {
                return MandrillError.Rejected;
            }

            // unexpected...
            return MandrillError.Unknown;
        }
        
    }
}